%%
% Group3_Ser05 part 2
%
% Argiris Papoudakis
%

close all;
clear;
clearvars -except c;
clc;


%% import dataset
Data = importdata('superconduct.csv');

[N m] = size(Data);

x1 = floor(0.6*N);
x2 = floor(0.8*N);

%% Smooth Data
% Data(:,1:end-1) = smoothdata(Data(:,1:end-1));

%% split Dataset
c = cvpartition(N, 'KFold', 5);

vector = 1:N;
[~, ~, v] = find(vector .* (c.test(1))');
Dchk = Data(v, :);
[~, ~, v] = find(vector .* (c.test(2))');
Dval = Data(v, :);
[~, ~, v] = find(vector .* (~(c.test(1) + c.test(2)))');
Dtrain = Data(v, :);


checkSize = size(Dchk,1);
avg = mean(Dchk(:,end));

% Number of features
% NF = [5 10 15 20];
NF = [20];
% Number of Rules
% NR = [5 10 15 20 25];
% We want to use subtractive clustering so we have to set 
% the number of radiis. 
NR = [0.49 0.35 0.28 0.26 0.24; ...
      0.66 0.40 0.33 0.25 0.22; ...
      0.77 0.43 0.32 0.25 0.24; ...
      0.71 0.40 0.26 0.23 0.22];

% NR = [0.22]


c = cvpartition(x1, 'KFold', 5);
[idx,weights] = relieff(Dtrain(:,1:m-1),Dtrain(:,m),500);

epochs = 50

meanError = zeros(1,1);

for i=1:length(NF)
    
    features = idx(1:NF(i));
    
    for j=1:length(NR)
        
        for z=1:5
            
           train = Dtrain(c.training(z), [features m]);
           test = Dtrain(c.test(z), [features m]);
           
           fismat1 = genfis2(train(:, 1:end-1), train(:,end), NR(i,j));
 
           [tmpM, tmpN] = size(fismat1.rule);
           clusters(i, j) = tmpN;
           
           fprintf('Found %d clusters.\n', tmpN);
           opt = anfisOptions('InitialFIS',fismat1,'EpochNumber',epochs);
           opt.DisplayANFISInformation=0;
           opt.DisplayErrorValues=0;
           opt.DisplayStepSize = 0;
           opt.DisplayFinalResults=0;
           opt.ValidationData=test;
           
           
           [fis, error, stepsize,chkFis,chkErr] = anfis(train,opt);

           error(z) = mean(chkErr);
           error(z)
        end
        
        meanError(i,j) = sum(error)/5;
        fprintf('Feature index %d Rule index %d [DONE].\n',i,j); 
        
    end
    
end


% x=1;
% y=1;

minError = min(meanError(:));
[x y] = find(meanError==minError);

NumberOfFeatures = NF(x);
NumberOfRules = NR(x,y);


ffeatures = [idx(1:NumberOfFeatures) m];

Dtrain2 = Dtrain(:,ffeatures);
Dval2 = Dval(:,ffeatures);
Dchk2 = Dchk(:,ffeatures); 


fismat2 = genfis2(Dtrain2(:, 1:end-1), Dtrain2(:,end), NR(x,y));

opt = anfisOptions('InitialFIS',fismat2,'EpochNumber',epochs);
opt.DisplayANFISInformation=0;
opt.DisplayErrorValues=0;
opt.DisplayStepSize = 0;
opt.DisplayFinalResults=0;
opt.ValidationData=Dval2;


[fis, error, stepsize,chkFis,chkErr] = anfis(Dtrain2,opt);
 

output = evalfis(Dchk2(:, 1:end-1), chkFis);

%% compute metrics
sum1 =0;
sum2=0;
for j=1:checkSize
    sum1 = sum1 + (Dchk2(j,end)-output(j))^2;
    sum2 = sum2 + (Dchk2(j,end)-avg)^2;
end
    
MSE = sum1/checkSize;
RMSE = sqrt(MSE);
R2 = 1 - sum1/sum2;
NMSE = sum1/sum2;
NDEI = sqrt(NMSE);



 %% learning curves
figure  

plot(error);
hold on;
plot(chkErr);
title(['Learning Curves'],'fontweight','bold');

xlabel('Iterations');
ylabel('Error');

legend('Training Error','Validation Error');


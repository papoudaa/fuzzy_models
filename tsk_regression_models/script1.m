%%
% Group3_Ser05 part 1
%
% Argiris Papoudakis
%
close all;
clear;
clc;

%% import CCPP dataset
Data = importdata('CCPP.dat');

[N m] = size(Data);

x1 = floor(0.6*N);
x2 = floor(0.8*N);

%% split Dataset
Dtrain = Data(1:x1,:);
Dval = Data(x1+1:x2,:);
Dchk = Data(x2+1:N,:);

%% Initialization of TSK models
model(1) = genfis1(Dtrain, 2, 'gbellmf', 'constant');
model(2) = genfis1(Dtrain, 3, 'gbellmf', 'constant');
model(3) = genfis1(Dtrain, 2, 'gbellmf', 'linear');
model(4) = genfis1(Dtrain, 3, 'gbellmf', 'linear');

avg = mean(Dchk(:,end));
checkSize = N-x2;
epochs = 110;

for i=1:4
    
    
    %% training
    [fis,trainError,stepSize,chkFIS,chkError] = anfis(Dtrain,model(i),[epochs, 0],0,Dval);
    
    %% testing
    output = evalfis(Dchk(:, 1:4), chkFIS);
    
    
    %% compute metrics
    sum1=0;
    sum2=0;
    
    for j=1:checkSize
        sum1 = sum1 + (Dchk(j,5)-output(j))^2;
        sum2 = sum2 + (Dchk(j,5)-avg)^2;
    end
    
    MSE(i) = sum1/checkSize;
    RMSE(i) = sqrt(MSE(i));
    R2(i) = 1 - sum1/sum2;
    NMSE(i) = sum1/sum2;
    NDEI(i) = sqrt(NMSE(i));
    
    %% graphs
    
    %% mfs
    figure    
    
    for j=1:4        
        [f,mf] = plotmf(chkFIS,'input',j);
        subplot(2,2,j)
        plot(f ,mf)
        title(['Input ' num2str(j)]) 
        
    end
    
    suptitle(['Model ' num2str(i)]);
    
    %% learning curves
    figure  
    
    plot(trainError);
    hold on;
    plot(chkError);
    title(['Learning Curves for Model ' num2str(i)],'fontweight','bold');
    
    xlabel('Iterations');
    ylabel('Error');
    
    legend('Training Error','Validation Error');
    
    %% prediction error
    predError = output - Dchk(:,5); 
    figure   
    plot(1:checkSize, predError);
    title(['Prediction Errors for Model ' num2str(i)],'fontweight','bold'); 
    xlabel('Samples');
    ylabel('Prediction Error');
    
end








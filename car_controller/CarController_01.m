%%
% author: Argiris Papoudakis 
% Car Controller 01 
%

clear all;
clc;

CarControl = readfis('CarControl_01modified2.fis');

%% initial point

xinit = 4.1;
yinit = 0.3;

dh(1) = 0.9;
dv(1) = 0.3;
x(1) = xinit;
y(1) = yinit;

%% desired point

xd = 10;
yd = 3.2;

%% initial angle

thetaIn = -90;
theta(1) = thetaIn;

u = 0.05;

i=1;

while (x<=xd)
    
    dtheta(i)=evalfis([dh(i) dv(i) theta(i)],CarControl);
    theta(i+1)=dtheta(i)+theta(i);
    
    x(i+1)=x(i)+cosd(theta(i+1))*u;  
    y(i+1)=y(i)+sind(theta(i+1))*u;
    
    %% compute new distances dh,dv
    if x(i)<5
        dv(i+1) = y(i);
    elseif x(i)< 6
        dv(i+1) = y(i)-1;
    elseif x(i)<7
        dv(i+1) = y(i)-2;
    else 
        dv(i+1) = y(i)-3;
    end
    
    dv(i+1) = min(dv(i+1), 1);
    
    
    if y(i)<1
        dh(i+1) = 5-x(i);
    elseif y(i)< 2
        dh(i+1) = 6-x(i);
    elseif y(i)<3
        dh(i+1) = 7-x(i);
    else 
        dh(i+1) = 1;
    end
    
    dh(i+1) = min(dh(i+1),1);    
    
    i= i + 1;
    
end


hold on;
r1=rectangle('Position',[5 0 5 1]);
set(r1, 'FaceColor',[0.7 0.7 0.7],'EdgeColor' ,[0.7 0.7 0.7]);
r2=rectangle('Position',[6 1 4 1]);
set(r2, 'FaceColor',[0.7 0.7 0.7],'EdgeColor' ,[0.7 0.7 0.7]);
r3=rectangle('Position',[7 2 3 1]);
set(r3, 'FaceColor',[0.7 0.7 0.7],'EdgeColor' ,[0.7 0.7 0.7]);


plot(x,y);
xlabel('x');
ylabel('y');








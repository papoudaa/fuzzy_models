%%
%    Satellite 09, Simulation2
%
%   Argiris Papoudakis, papoudaa@ece.auth.gr
%
clear;
clc;

flc = readfis('flc.fis');

model = 'SatelliteSimulation2';
load_system(model);


% Set final parameters
set_param([model '/FZ-PI/K'],'Gain','9');
set_param([model '/FZ-PI/ke'], 'Gain', '1');
set_param([model '/FZ-PI/kd'],'Gain', '0.4');

simOutput = sim(model, 'SaveOutput','on');

simout = simOutput.get('simout');
time = simout.time;

out = simout.Data;
reference = out(:,1);
output = out(:,2);

hold on
plot(time,reference);
plot(time,output);

xlabel('time(secs)');
ylabel('Degrees');
legend('Reference Signal','Output Response');

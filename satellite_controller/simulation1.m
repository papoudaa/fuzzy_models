%%
%  Satellite09, Simulation 1
% 
%  Argiris Papoudakis, papoudaa@ece.auth.gr 
%

clear;
clc;

s = tf('s');

%% PI Controller

pole = 1.5; 
k = 20;

sysOpen = k*(s+pole)/(s*(s+1)*(s+9));
sysClosed = feedback(sysOpen, 1,-1);

%% FZ-PI Controller

flc = readfis('flc.fis');

% initial parameters 

kp = 2;
ki = 3;

a = kp/ki;
K = kp/a;

%% Load simulation model 

model = 'SatelliteSimulation1';
load_system(model);

% Set initial parameters
set_param([model '/FZ-PI/K'],'Gain',num2str(K));
set_param([model '/FZ-PI/ke'], 'Gain', '1');
set_param([model '/FZ-PI/kd'],'Gain', num2str(a));


simOutput = sim(model,'SaveOutput','on');
% simOutput = sim(model);

outInitial = simOutput.get('simout');
yInitial = outInitial.Data;
tInitial = simOutput.get('tout');
figure
plot(tInitial,yInitial)

initialInfo = stepinfo(yInitial,tInitial);

% Set final parameters
set_param([model '/FZ-PI/K'],'Gain','9');
set_param([model '/FZ-PI/ke'], 'Gain', '1');
set_param([model '/FZ-PI/kd'],'Gain', '0.4');

simOutput = sim(model, 'SaveOutput','on');

outFinal = simOutput.get('simout');
yFinal =outFinal.Data;
tFinal = simOutput.get('tout');
figure
plot(tFinal,yFinal);

finalInfo=stepinfo(yFinal,tFinal);

sysOpen = k*(s+pole)/(s*(s+1)*(s+9));
sysClosed = feedback(sysOpen, 1,-1);

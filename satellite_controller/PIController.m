%%
%  Satellite09, Simulation 1
% 
%  Argiris Papoudakis, papoudaa@ece.auth.gr 
%

clc;
clear;

pole = 1.5; 

s = tf('s');
sysOpen = (s+pole)/(s*(s+1)*(s+9));

rlocus(sysOpen);

for k=0:100
    
    sysOpen = k*(s+pole)/(s*(s+1)*(s+9));
    sysClose = feedback(sysOpen, 1, -1);
    str = stepinfo(sysClose);
    
    riseTime(k+1) = str.RiseTime;
    overshoot(k+1) = str.Overshoot;

end

k=0:100;

riseTimeLimit(1:100) = 1.2;
overshootLimit(1:100) = 10;

figure;
plot(riseTimeLimit);
hold on
plot(overshootLimit);
plot(k,riseTime);


plot(k,overshoot);
legend('Risetime 1.2 secs limit','Overshoot 10% limit','risetime','overshoot')
xlabel('K');


figure;
k=20;
sysOpen = ( k*s+k*pole )/(s*(s+1)*(s+9));
sysClose = feedback(sysOpen, 1, -1);
step(sysClose);

finalStruct = stepinfo(sysClose);

finalRiseTime = finalStruct.RiseTime;
finalOvershoot = finalStruct.Overshoot;

kp = k/10;
ki = kp * pole;

% % plot(sysClose);
% % lsim(sysClose);

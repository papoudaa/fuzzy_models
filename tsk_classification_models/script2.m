%%
% Group4_Ser02, Part 2
%
% Argiris Papoudakis 
%

clear;
clc;
close all;


%% import Dataset (isolet.dat, 7797 x 618)
Data = importdata('isolet.dat');

Data(:,1:end-1) = normalize(Data(:,1:end-1),'range');
Data(:,1:end-1) = smoothdata(Data(:,1:end-1),'SmoothingFactor',0.05);

% properties of the dataset
[N m] = size(Data);
classes = unique(Data(:, m));
NumberOfClasses = size(classes,1);


%% Split Dataset
c = cvpartition(Data(:,end), 'KFold', 5);

vector = 1:N;
[~, ~, temp1] = find((vector .* (c.test(1))'));
Dchk = Data(temp1, :);
[~, ~, temp2] = find((vector .* (c.test(2))'));
Dval = Data(temp2, :);
[~, ~, temp3] = find((vector .* (~(c.test(1)+c.test(2)))'));
Dtrain = Data(temp3, :);

%% Number of Features
NF = [5 10 15 20];

%% Number of Rules
% We want to use subtractive clustering (genfis2) so we have to set 
% the number of radiis. We select the radiis in order to achieve the
% desired number of clusters.
% NR = [4 8 12 16 20];

NR = [0.8, 0.7, 0.5, 0.23, 0.22; ...
         0.8, 0.7, 0.5, 0.3, 0.28; ...
         0.8, 0.7, 0.5, 0.35, 0.32; ...
         0.8, 0.7, 0.5, 0.42, 0.37];



x1 = floor(0.6*N);
x2 = floor(0.8*N);

c = cvpartition(x1, 'KFold', 5);

%% Relieff for feature selection
[idx,weights] = relieff(Dtrain(:,1:m-1),Dtrain(:,m),500);

meanError = zeros(4,5);

for i=1:length(NF)
    
    %% features selection
    features = idx(1:NF(i));
   
    for j=1:length(NR)
        
        %% 5-folds
        for z = 2:2
            
            train = Dtrain(c.training(z), [features m]);
            test = Dtrain(c.test(z), [features m]);
            
            fismat1 = genfis2(train(:, 1:end-1), train(:,end), NR(i,j));
            
            clusters(i, j) = length(fismat1.output.mf);
            fprintf('Found %d clusters.\n', clusters(i,j));
            
            [fis,error,stepsize,chkFis,chkErr] = anfis(train,fismat1,[80 0],0,test);
            
            error(z) = mean(chkErr);
            
        end
        
        meanError(i,j) = mean(error, 'omitnan');
        fprintf('Feature index %d Rule index %d [DONE].\n',i,j); 
        
        
    end
    
    
end


%% find optimal features and radii
minError = min(meanError(:));
[x y] = find(meanError==minError);

NumberOfFeatures = NF(x);
NumberOfRules = NR(x,y);

ffeatures = [idx(1:NumberOfFeatures) m];

%% new dataset
Dtrain2 = Dtrain(:,ffeatures);
Dval2 = Dval(:,ffeatures);
Dchk2 = Dchk(:,ffeatures); 


fismat2 = genfis2(Dtrain2(:, 1:end-1), Dtrain2(:,end), NR(y));
[fis, error,~,chkFis,chkErr] = anfis(Dtrain2,fismat2, [80 0], 0,Dval2);

% evaluation
output = evalfis(Dchk2(:, 1:end-1), chkFis);
output = round(output);

checkSize = size(Dchk2,1);

for j=1:checkSize
       
       if (output(j)<1)
           output(j) = 1;
       elseif (output(j)>26)
           output(j) = 26;
       end
        
end

 %% learning curves
figure  

plot(error);
hold on;
plot(chkErr);
title(['Learning Curves'],'fontweight','bold');

xlabel('Iterations');
ylabel('Error');

legend('Training Error','Validation Error');

%% compute metrics
    
% error matrix
errorMatrix = confusionmat(Dchk2(:, end), output)';

% overall accuracy
accuracy = sum(diag(errorMatrix))/checkSize;
Sum1 = 0;


for j=1:NumberOfClasses

    % producer accuracy
    PA(j) = errorMatrix(j,j)/sum(errorMatrix(:,j));

    % user accuracy
    UA(j) = errorMatrix(j,j)/sum(errorMatrix(j,:));

    Sum1 = Sum1 + sum(errorMatrix(:,j))*sum(errorMatrix(j,:));  

end


k = (checkSize*sum(diag(errorMatrix))-Sum1)/(checkSize^2-Sum1);



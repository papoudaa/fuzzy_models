%%
% Group4_Ser02, Part 1
%
% Argiris Papoudakis
%

clear;
close all;
clc;

%% load Dataset (avila.txt, 20867 x 11)
Data = importdata('avila.txt');

%% preprocessing
Data(:,1:end-1) = [normalize(Data(:,1:end-1),'range');
Data = rmoutliers(Data,'quartiles','ThresholdFactor',20);

[N m] = size(Data);

classes = unique(Data(:, m));
NumberOfClasses = size(classes,1);

c = cvpartition(Data(:,end), 'KFold', 5);

vector = 1:N;
[~, ~, temp1] = find((vector .* (c.test(1))'));
Dchk = Data(temp1, :);
[~, ~, temp2] = find((vector .* (c.test(2))'));
Dval = Data(temp2, :);
[~, ~, temp3] = find((vector .* (~(c.test(1)+c.test(2)))'));
Dtrain = Data(temp3, :);

checkSize = size(Dchk,1);

%% Number of Rules
%%NR = [4 8 12 16 20]
NR = [0.26 0.185 0.145 0.126 0.12];

%% TSK 5 models
for i=1:5
    
    
    model(i) = genfis2(Dtrain(:, 1:end-1), Dtrain(:, end), NR(i));
    
    [tx ty] = size(model(i).rule);
    length(model(i).output.mf)
    
    % change output from linear to constant
    for j=1:length(model(i).output.mf)
       model(i).output.mf(j).type = 'constant';
       model(i).output.mf(j).params = model(i).output.mf(j).params(end);
    end
    
    
    %% training
    [fis, trainError, stepsize,chkFis,chkErr] = anfis(Dtrain,model(i),[120 0],0,Dval);
    
    %% TESTING 
    output = evalfis(Dchk(:, 1:end-1),chkFis);
    output = round(output);
    
    for j=1:checkSize
       
       if (output(j)<1)
           output(j) = 1;
       elseif (output(j)>12)
           output(j) = 12;
       end
        
    end
    
    
    %% learning curves
    figure  
    plot(trainError);
    hold on;
    plot(chkErr);
    title(['Learning Curves for Model ' num2str(i)],'fontweight','bold')  
    
    xlabel('Iterations')
    ylabel('Error')
    
    legend('Training Error','Validation Error')
    
    %% compute metrics
    
    % error matrix
    errorMatrix = confusionmat(Dchk(:, end), output)';
    
    % overall accuracy
    OA(i) = sum(diag(errorMatrix))/checkSize;
    Sum1 = 0;
    for j=1:NumberOfClasses
        
        %producer accuracy
        PA(i,j) = errorMatrix(j,j)/sum(errorMatrix(:,j));
        
        %user accuracy
        UA(i,j) = errorMatrix(j,j)/sum(errorMatrix(j,:));
        
        Sum1 = Sum1 + sum(errorMatrix(:,j))*sum(errorMatrix(j,:));  
        
    end
    
    
     k(i) = (checkSize*sum(diag(errorMatrix))-Sum1)/(checkSize^2-Sum1);
     
     
    
end



